ARG VERSION=latest

FROM httpd:${VERSION}

COPY install.sh /root/

RUN /bin/bash /root/install.sh