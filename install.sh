#!/bin/bash
sed -i -E 's/#LoadModule (deflate|expires|http2|proxy|proxy_connect|proxy_fcgi|proxy_http|proxy_http2|proxy_wstunnel|rewrite|sed|ssl|substitute)_module/LoadModule \1_module/g' conf/httpd.conf

cat <<EOF >> conf/httpd.conf
<VirtualHost *:80>
	ServerName localhost
	DocumentRoot /var/www/html
	DirectoryIndex index.php index.html
	<Directory "/var/www/html">
		AllowOverride All
		Require all granted
	</Directory>
</VirtualHost>
EOF

cat <<EOF > /usr/local/bin/httpd-foreground
#!/bin/sh
set -e

if [ -n "\${DOCUMENT_ROOT_SUBDIR}" ]
then
	sed -i -E "s#DocumentRoot /var/www/html.*#DocumentRoot /var/www/html/\${DOCUMENT_ROOT_SUBDIR}#g" conf/httpd.conf
fi

if [ -n "\${SERVICE_PHP_FPM}" ]
then
	sed -i -E 's#</VirtualHost>#	<FilesMatch ".+\.ph(p[3457]?|t|tml)$">'"\n"'		SetHandler "proxy:fcgi://'"\${SERVICE_PHP_FPM}"':9000"'"\n"'	</FilesMatch>'"\n"'</VirtualHost>#g' conf/httpd.conf
fi

# Apache gets grumpy about PID files pre-existing
rm -f /usr/local/apache2/logs/httpd.pid

exec httpd -DFOREGROUND
EOF

chmod +x /usr/local/bin/httpd-foreground
